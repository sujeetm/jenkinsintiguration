resource "aws_instance" "web_server" {
  ami           = "ami-0ab4d1e9cf9a1215a"
  instance_type = "t2.micro"
  key_name      = "terraform"

  tags = {
    OwnerName   = "Ansh"
    Environment = "dev21"
  }
}
output "webserver" {
  value = "${aws_instance.web_server.id}"
}

resource "aws_s3_bucket" "back1232334" {
  bucket = "my-tf-bucket6636464"
  acl    = "private"
  tags = {
    Name        = "Sujeet_bucket"
    Environment = "Dev"
  }
}

